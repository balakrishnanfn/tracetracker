# [Home](README.md)
# Swipe to referesh not called more than once while using RxJava
 
## Issue Description
I have used RxJava with Retrofit for network calls. I have a API endpoint that is paginated by page number i.e 1,2,3... Pagination API call works fine while scrolling. When trying to reload the listing swipe to refresh API call is called only once. 
* * *

```
 val client: ApiInterfacekt = RetrofitClientkt().get().create(ApiInterfacekt::class.java)
        val h: HashMap<String, String> = HashMap(3)
        return publishSubject.distinctUntilChanged()
                .flatMapSingle {  s ->
                    h["page"] = s.toString()
                    Log.d(TAG, "loadFavourite: called $s")
                    client.getSingleRequest("list", h)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                }
                .subscribeWith(observer1)
```
* * *

## Solution

The problem with above mentioned is I have used `publishSubject.distinctUntilChanged()` which only allow calls if only the page number is different. Remove `distinctUntilChanged()`
 everything will work fine
 * * *

```
 val client: ApiInterfacekt = RetrofitClientkt().get().create(ApiInterfacekt::class.java)
        val h: HashMap<String, String> = HashMap(3)
        return publishSubject
                .flatMapSingle {  s ->
                    h["page"] = s.toString()
                    Log.d(TAG, "loadFavourite: called $s")
                    client.getSingleRequest("list", h)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                }
                .subscribeWith(observer1)
```


* * *

>   Note
> -----------
>   * If API Calls are not wotking with RxJava; First make sure function that contiains the API call is being called
>   * Then check the Operators that are used while making the API call
* * *
Tags : [#Android]() [#RxJava]()
* * *
