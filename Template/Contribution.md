# Contribution Guide ⚡️💥
* Create a `.md` file in respective folder with [this](Template/sample.md)template under the directory of user issue type 💥; i.e RxJava, Gradle and etc..,
* If the directory is not present dont be shy to create a new one 🥳.
* Add link to your issue in the [Home](README.md) file under respective topics 😊
Make a pull request 🤩