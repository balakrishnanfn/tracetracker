# [Home](README.md)
Issue title
======== 
### Issue Description
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi arcu orci, eleifend sit amet lacus eu, condimentum laoreet ligula. Phasellus commodo dui at lacus rhoncus volutpat. Vivamus dapibus, erat id congue ullamcorper, magna diam vestibulum nibh, eu imperdiet elit lectus in eros. Aliquam lobortis feugiat turpis, id maximus nibh tincidunt a. Fusce quam diam, mattis non euismod ac, tincidunt nec risus. Ut laoreet est sed orci auctor sodales in a leo. Etiam volutpat urna eget hendrerit dictum. Morbi sagittis efficitur suscipit. Duis ornare diam metus, id ornare ex vehicula at. In molestie porta vehicula. Cras a metus imperdiet, efficitur turpis vitae, tristique est. Praesent eu nunc eget orci lobortis dignissim 
* * *

```
/** Sample code with issues
*
*/
class SomeClass{
    SomeClass(){
        ...
    }
    ...
    ...
}
```
* * *

### Proposed solution

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi arcu orci, eleifend sit amet lacus eu,condimentum laoreet ligula. Phasellus commodo dui at lacus rhoncus volutpat. Vivamus dapibus, erat id congue ullamcorper, magna diam vestibulum nibh, eu imperdiet elit lectus in eros. 
* * *
```
/** Sample code with solution
*
*/
class SomeClass{
    SomeClass(){
        ...
    }
    ...
    ...
}
```
* * *

>  # Note
> * Vivamus dapibus, erat id congue ullamcorper, magna diam vestibulum nibh, eu imperdiet elit lectus in eros.
> * Morbi arcu orci, eleifend sit amet lacus eu,condimentum laoreet ligula.  

* * *
Tags : [#Android]() [#RxJava]()
* * *
