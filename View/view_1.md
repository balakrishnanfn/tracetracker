# [Home](README.md)
How to find a view uniquely in recycler view
======== 
### Issue Description
I have a Recyclerview with images in each viewholder. I have to store a image view in a hashmap using the `view id` as a key for identifying each view for later use (setting image). I used `imageView.getId()`. But in Recyclerview these view ids are same.
* *  *

```
public ViewHolder onBindViewHolder(ViewHolder sampleHolder, int position){
// Map does not allow duplicate kwy but duplicate values.
// Thus map will ignore next entries, Since id of the view is same
map.put(sampleholder.imageView.getId(),sampleholder.imageView)
}
```
* *  *

### Proposed solution

Instead of using `view.getId()` use `java.lang.System.identityHashCode(view);` this will give hashCode for a view
* *  *

```
public ViewHolder onBindViewHolder(ViewHolder sampleHolder, int position){
    // Here we get unique hash code. No Duplicates.
    map.put(java.lang.System.identityHashCode(sampleHolder.imageView),sampleholder.imageView);
}
```
* * *
Tags : [#Android]() [#View]()
* * *
