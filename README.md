# TraceTracker

TraceTracker is a collection of isssues faced and solution given on daily basis and to enrich the solutions. 

Topics: 
## RxJava 
1. [Observer not observing once onError is called](RxJava/observer.md)
2. [Swipe to referesh not called more than once while using RxJava](RxJava/swipe_issue.md)

## Gradle 
1. [Excluding packages](Gradle/exclude_module.md)
2. [Naming and Versioning]

## View
1. [How to find a view uniquely in recycler view](View/view_1.md)

 ***Contribute to TraceTracker to save   time and effort. Simply read our [Contribution Guide ⚡️💥](Template/Contribution.md) and get started! *** 🥳
